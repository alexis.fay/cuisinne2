package cuisinne2;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="recettes")
public class Recette implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String nom;
	private String description;
	private String classification;
	private Double prix;
	public Recette(String nom, String description, String classification, Double prix) {
		super();
		id=0;
		this.nom = nom;
		this.description = description;
		this.classification = classification;
		setPrix(prix);
	}
	
	public Recette() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(length=50, nullable=false, unique=true)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@Column(nullable=true)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(nullable=false)
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	
	@Column(nullable=false ,  precision=5, scale=2)
	public Double getPrix() {
		return prix;
	}
	public void setPrix(Double prix) {
		if (prix<0) {
			throw new IllegalArgumentException ("jours ne doit pas être neg");
		} else {
			this.prix = prix;
		}
	}
	@Override
	public String toString() {
		return id + ", " + nom + " : , description=" + description + ", classification="
				+ classification + ", prix=" + prix + "]";
	}
	
	
}
