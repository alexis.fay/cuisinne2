package cuisinne2;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reservations")
public class Reservation implements Serializable {

	private static final long serialVersionUID = 1L;
	private LocalDateTime date;
	private String nom;
	private int nombre;
	private int id;
	
	public Reservation(LocalDateTime date, String nom, int nombre) {
		super();
		this.date = date;
		this.nom = nom;
		this.nombre = nombre;
		id=0;
	}
	public Reservation() {
		super();
	}
	@Column(nullable = false)
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	@Column(length = 40, nullable = false)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Column(nullable = false)
	public int getNombre() {
		return nombre;
	}
	public void setNombre(int nombre) {
		this.nombre = nombre;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Reservation [date=" + date + ", nom=" + nom + ", nombre=" + nombre + "]";
	}
	
	
	
}
