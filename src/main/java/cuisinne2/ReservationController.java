package cuisinne2;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReservationController {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private static final Logger logger = LoggerFactory.getLogger(ReservationController.class);

	private ReservationsService reservationsService;

	@Autowired
	public void setReservationsService(ReservationsService reservationsService) {
		this.reservationsService = reservationsService;
	}

	@RequestMapping(path = "/reservations", method = RequestMethod.POST)
	public void ajouter(@RequestBody Reservation reservation) {
		reservationsService.ajouterReservation(reservation);
		logger.warn("Reservation envoyée");
	}

	@RequestMapping(path = "/reservations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Reservation> recuperer() {
	Session session = sessionFactory.openSession();
	List<Reservation> reservationListe = session.createQuery("from Reservation ORDER BY date", Reservation.class).list();
	session.close();
	return reservationListe;
	}
	
	// id est un paramètre dans l'URL ; \d+ => nombre (1 ou pls chiffres)
	@RequestMapping(path="/remove/{id:\\d+}", method=RequestMethod.POST)
	public void supprimer(@PathVariable("id") int id) {
		reservationsService.supprimerReservation(id);
	}
}
