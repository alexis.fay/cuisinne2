package cuisinne2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecetteService {

	private SessionFactory sessionFactory;
	
	public void enregistrerRecette (String nom, String description, String classification, double prix) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try {
		Recette r = new Recette (nom, description, classification, prix);
		session.save(r);
		tx.commit();
		} catch (Exception e){
			System.err.println("Erreur : "+e);
			tx.rollback();
		}
		session.close();
	}
	
	// fonction non utilisée
	public void supprimerRecette(int id) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		try {
			Recette recette = session.load(Recette.class, id);
			session.delete(recette);
			tx.commit();
		} catch (Exception e) {
			System.err.println("Erreur : "+e);
			tx.rollback();
		}
		session.close();
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	} 
}
