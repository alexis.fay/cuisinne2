package cuisinne2;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HibernateConfiguration {
	
	@Bean
	public SessionFactory sessionFactory() {
		Properties options = new Properties();
		options.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
		options.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
		options.put("hibernate.connection.url", "jdbc:mysql://localhost/voyages");
		options.put("hibernate.connection.username", "voyages");
		options.put("hibernate.connection.password", "password");
		options.put("hibernate.hbm2ddl.auto", "update");
		options.put("hibernate.show_sql", "true");
		SessionFactory factory = new org.hibernate.cfg.Configuration().
				addProperties(options).
				addAnnotatedClass(Recette.class).
				addAnnotatedClass(Reservation.class).
				buildSessionFactory() ;
				
		return factory;
	}
}
