package cuisinne2;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReservationsService {

	private static final Logger logger = LoggerFactory.getLogger(ReservationController.class);

	private SessionFactory sessionFactory;

	public ReservationsService() {

	}

	public void ajouterReservation(Reservation reservation) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try {

			session.save(reservation);
			tx.commit();
		} catch (Exception e) {
			System.err.println("Erreur : " + e);
			tx.rollback();
		}
		session.close();

	}

	public void supprimerReservation(int id) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			Reservation d = session.get(Reservation.class, id);
			session.delete(d);
			tx.commit();
		} catch (Exception e) {
			System.err.println("Erreur : " + e);
			tx.rollback();
		}
		session.close();

	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
