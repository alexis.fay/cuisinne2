package cuisinne2;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RecetteController {

	private RecetteService recetteService;

	private SessionFactory sessionFactory;

	@RequestMapping(path = "/recette", method = RequestMethod.GET)
	public String afficher(Model model) {

		Session session = sessionFactory.openSession();
		// on récupère les recettes classées dans l'ordre "entrées, puis plats, puis desserts"; puis par ordre alphabétique
		List<Recette> recettesListe = session.createQuery(
				"from Recette ORDER BY case when classification = 'entree' then 1"
						+ "              when classification = 'plat' then 2"
						+ "              when classification = 'dessert' then 3 end asc"
						+ ", nom " ,
				Recette.class).list();
		session.close();
		model.addAttribute("recettesListe", recettesListe);

		return "recette";
	}

	@RequestMapping(path = "/recette", method = RequestMethod.POST)
	public String recuperer(@RequestParam("nom") String nom, @RequestParam("description") String description,
			@RequestParam("classification") String classification, @RequestParam("prix") double prix, Model model) {

		recetteService.enregistrerRecette(nom, description, classification, prix);
		return afficher(model);
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public RecetteService getNamibieService() {
		return recetteService;
	}

	@Autowired
	public void setNamibieService(RecetteService recetteService) {
		this.recetteService = recetteService;
	}
}