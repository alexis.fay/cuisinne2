<head>
<meta charset="UTF-8">
<title>cuisinne2</title>
<link rel="stylesheet" href="index.css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
	crossorigin="anonymous"></script>
</head>

<body>
	<header class="couleurFond">
	
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container-fluid">
				<a class="navbar-brand" class="active" href="index.jsp">LaCuisinne</a>
				<button class="navbar-toggler" type="button"
					data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02"
					aria-controls="navbarTogglerDemo02" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item"><a class="nav-link active"
							aria-current="page" href="http://localhost:8080/cuisinne/index.jsp">Accueil</a></li>
						<li class="nav-item"><a class="nav-link active"
							href="http://localhost:8080/cuisinne/chrono.jsp">Minuteur</a></li>
						<li class="nav-item"><a class="nav-link active"
							href="http://localhost:8080/cuisinne/decongelation">Décongélation</a></li>
						<li class="nav-item"><a class="nav-link active"
							href="http://localhost:8080/cuisinne/conversion">Conversions</a></li>
						<li class="nav-item"><a class="nav-link active"
							href="recette">Recette</a><li>
						<li class="nav-item"><a class="nav-link active"
							href="reservations.jsp">Reservez</a><li>
					</ul>
				</div>
			</div>
		</nav>
	</header>