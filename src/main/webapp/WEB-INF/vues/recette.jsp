<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<!DOCTYPE html>

<html>
<%@ include file="head.jsp"%>

<h1 class="center">Formulaire de création de repas</h1>


<form class="center" method="post">
	<p>
		<label>Nom<br><input type="text" name="nom"></label>
	</p>
	<p>
		<label>Description<br><textarea rows="4" cols="30"
				name="description"></textarea></label>
	</p>
	<p>
		<label>Classification<br><select name="classification">
				<option value="Entree">Entrée</option>
				<option value="Plat">Plat principal</option>
				<option value="Dessert">Dessert</option>
		</select></label>
	</p>
	<p>
		<label>Prix<br><input type="number" name="prix"></label>
	</p>
	<p>
		<input type="submit">
	</p>

</form>


<c:if test="${!recettesListe.isEmpty()}">
	<h2 class="center">Vos recettes :</h2>

	<table class="center">
		<thead class="tableau">
			<tr>
				<th class="tableau">Classification</th>
				<th class="tableau">Nom</th>
				<th class="tableau">Prix</th>
				<th class="tableau">Description</th>
			</tr>
		</thead>
		<tbody class="tableau">
			<c:forEach items="${recettesListe}" var="recette">
				<tr>
					<td class="tableau">${recette.getClassification()}</td>
					<td class="tableau">${recette.getNom()}</td>
					<td class="tableau">${recette.getPrix()}€</td>
					<td class="tableau">${recette.getDescription()}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>



<%@ include file="footer.jsp"%>
</body>

</html>