<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<!DOCTYPE html>

<html>

<script src="reservations.js" defer></script>
<%@ include file="WEB-INF/vues/head.jsp"%>

<h1 class="center">Reservez une table</h1>

<form class="center" method="post">
	<p>
		<label>Votre nom : <br> <input type="text"
			id="nom"></label>
	</p>
	<p>
		<label>Nombres de personne : <br> <input type="number"
			id="nombre" min="1" max="30"></label>
	</p>
	<p>
		<label>Date et heure : <br> <input type="datetime-local"
			id="date"></label>
	</p>
	<input type="button" value="Reserver" id="button">

	<br><br>
	<table id="table" class="center"></table>
	

</form>

<%@ include file="WEB-INF/vues/footer.jsp"%>
</body>

</html>