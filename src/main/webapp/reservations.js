// permet de remplir le tableau avec la liste des réservations 
// tirée de la base de données par le service Rest
function afficher() {
	let xhr = new XMLHttpRequest()
	xhr.open("GET", "http://localhost:8082/reservations")
	xhr.onreadystatechange = function() {

		if (this.status == 200 && this.readyState == 4) {
			let listeReservations = JSON.parse(this.responseText)
			let html = "<thead><tr><th>Nom</th><th>Date</th><th>Nombre de convives</th></tr></thead>"
			for (let res of listeReservations) {
				
				let date = new Date(res["date"])
				let dateString = date.toLocaleDateString("fr") + " " + date.toLocaleTimeString("fr")
				
				html += "<tbody><tr><td>" + res["nom"] + "</td><td>" +dateString + "</td><td>" + res["nombre"] + "</td><td><input onclick='supprimer(" + res["id"] + ")' type='button' value='Supprimer'></td></tr></tbody>"
			}
			table.innerHTML = html
		}
	}
	xhr.send()
}

// retire une réservation de la base de donnée via Rest à partir de l'id
function supprimer(id) {
	let xhr = new XMLHttpRequest()
	xhr.open("POST", "http://localhost:8082/remove/" + id)
	// on attend que la requête soit bien terminée, puis on ré-affiche
	xhr.onreadystatechange = function() {
		if (this.status == 200 && this.readyState == 4) {
			afficher()
		}
	}
	xhr.send()
}

let nom = document.getElementById("nom")
let nombre = document.getElementById("nombre")
let date = document.getElementById("date")
let button = document.getElementById("button")
let table = document.getElementById("table")


// bouton Réserver => envoie les données vers le service Rest pour ajouter à la base de données "reservations"
button.addEventListener("click", function() {
	let xhr = new XMLHttpRequest()
	xhr.open("POST", "http://localhost:8082/reservations")
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
	// on attend que la requête soit bien terminée, puis on ré-affiche
	xhr.onreadystatechange = function() {
		if (this.status == 200 && this.readyState == 4) {
			afficher()
		}
	}
	// on envoie les données du formulaire au format JSON
	xhr.send(JSON.stringify({
		"date": date.value,
		"nom": nom.value,
		"nombre": nombre.value
	}))
})

afficher()